class TodoList {

    /**
     * Add new task 
     */
    add_task() {
        let add_task = document.querySelector('#push');
        let new_task = document.querySelector('#newtask input');
        let list_task = document.querySelector('#tasks');

        add_task.onclick = () => {
            if (new_task.value.trim() === "") {
                alert("Please enter a task to be done")
            } else {
                list_task.innerHTML += `
                <div class="task">
                    <span>${new_task.value.escapeHtml()}</span>
                    <button class="delete"><i class="far fa-trash-alt"></i></button>
                </div>`;
                new_task.value = "";
                this.delete_task();
                this.task_completed();
            }
        }
    }

    /**
     * Delete one task
     */
    delete_task() {
        let current_tasks = document.querySelectorAll(".delete");
        for (let i = 0; i < current_tasks.length; i++) {
            current_tasks[i].onclick = function() {
                this.parentNode.remove();
            }
        }
    }

    /**
     * Mark one task as completed
     */
    task_completed() {
        let tasks = document.querySelectorAll(".task");
        for (var i = 0; i < tasks.length; i++) {
            tasks[i].onclick = function() {
                this.classList.toggle('completed');
            }
        }
    }
}

String.prototype.escapeHtml = function() {
    var tagsToReplace = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;'
    };
    return this.replace(/[&<>]/g, (tag) => {
        return tagsToReplace[tag] || tag;
    });
};